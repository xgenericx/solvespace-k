ksolvespace
==========

This repository contains an unofficial improved repository of [SolveSpace][].

[solvespace]: http://solvespace.com

License
-------

SolveSpace is distributed under the terms of the [GPL3 license](COPYING.txt).